import os

import launch_ros.actions
from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.substitutions import LaunchConfiguration
from launch_ros.substitutions import FindPackageShare
from launch.actions import OpaqueFunction, DeclareLaunchArgument
from ament_index_python.packages import get_package_share_directory
from launch.substitutions import PathJoinSubstitution, TextSubstitution
from launch.launch_description_sources import PythonLaunchDescriptionSource


launch_args = [
    DeclareLaunchArgument(name='verbose',         default_value='false',         
                            description='Show additional gazebo info.'),
    DeclareLaunchArgument(name='use_sim_time',    default_value='true',         
                            description='Run simulation time if true.'),
    DeclareLaunchArgument(name='model_ns',        default_value='actros_ns',    
                            description='Model namespace in ros.'),
    DeclareLaunchArgument(name="model_name",      default_value='example_name', 
                            description="Model name in gazebo."),      
    DeclareLaunchArgument(name='world_file_name', default_value='car.world',  
                            description='World name in alpha_gazebo_worlds/worlds.'),
    DeclareLaunchArgument(name='xacro_dir_name',  default_value='actros',       
                            description='xacro dir name in alpha_gazebo_vehicle/urdf.'),
    DeclareLaunchArgument(name='models_count',    default_value='1',            
                            description='')

]


def spawn_models_launch(context):
    models_count = int(LaunchConfiguration('models_count').perform(context))
    
    base_model_name = str(LaunchConfiguration('model_name').perform(context)); 
    base_model_ns   = str(LaunchConfiguration('model_ns').perform(context));

    output_list = []

    for idx in range(models_count):
        model_name = base_model_name + "_" + str(idx)
        model_ns   = base_model_ns   + "_" + str(idx)

        spawn_xacro = IncludeLaunchDescription(
            PythonLaunchDescriptionSource([
                PathJoinSubstitution([
                    FindPackageShare('alpha_gazebo_vehicles'),
                    'launch',
                    'spawn_xacro.launch.py'
                ])
            ]),
            launch_arguments={
                'model_name': model_name,
                'model_ns': model_ns,
                'x': str(10 * idx),
                'y': "0.0",
                'z': "0.0",
                'r': "0.0",
                'p': "0.0",
                'Y': "0.0"

            }.items()
        )

        output_list.append(spawn_xacro)

    return output_list


def generate_launch_description():
    gazebo = IncludeLaunchDescription(
                PythonLaunchDescriptionSource([os.path.join(
                    get_package_share_directory('alpha_gazebo_worlds'), 
                    'launch', 'alpha_gazebo_worlds.launch.py')]),
             )
  
    spawn_models = OpaqueFunction(function = spawn_models_launch)


    ld = LaunchDescription(launch_args)
    ld.add_action(spawn_models)
    ld.add_action(gazebo)

    return ld  