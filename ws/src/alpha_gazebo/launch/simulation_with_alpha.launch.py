import os

import launch_ros.actions
from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.substitutions import LaunchConfiguration
from launch_ros.substitutions import FindPackageShare
from launch.actions import OpaqueFunction, DeclareLaunchArgument
from ament_index_python.packages import get_package_share_directory
from launch.substitutions import PathJoinSubstitution, TextSubstitution
from launch.launch_description_sources import PythonLaunchDescriptionSource


launch_args = [
    DeclareLaunchArgument(name='verbose',         default_value='true',         
                            description='Show additional gazebo info.'),
    DeclareLaunchArgument(name='use_sim_time',    default_value='true',         
                            description='Run simulation time if true.'),
    DeclareLaunchArgument(name='model_ns',        default_value='',    
                            description='Model namespace in ros.'),
    DeclareLaunchArgument(name="model_name",      default_value='actros', 
                            description="Model name in gazebo."),      
    DeclareLaunchArgument(name='world_file_name', default_value='car.world',  
                            description='World name in alpha_gazebo_worlds/worlds.'),
    DeclareLaunchArgument(name='xacro_dir_name',  default_value='actros',       
                            description='xacro dir name in alpha_gazebo_vehicle/undf.'),
    DeclareLaunchArgument(name='serial_port',     default_value='/dev/ALPHA_TEST_VEHICLE_SIDE',            
                            description='Serial port name for using alpha and alpha emu.')
]


def start_alpha_emu(context):
    serial_port = str(LaunchConfiguration('serial_port').perform(context))
    model_ns    = str(LaunchConfiguration('model_ns').perform(context))

    alpha_emu = launch_ros.actions.Node(
            package='alpha_emu',
            namespace=model_ns,
            executable='AlphaEmu',
            output="screen",
            parameters=[  
                {
                "serial_port": serial_port
                }
            ]
        )
    return [alpha_emu]


def spawn_model(context):
    model_name = str(LaunchConfiguration('model_name').perform(context))
    model_ns   = str(LaunchConfiguration('model_ns').perform(context))

    spawn_xacro = IncludeLaunchDescription(
            PythonLaunchDescriptionSource([
                PathJoinSubstitution([
                    FindPackageShare('alpha_gazebo_vehicles'),
                    'launch',
                    'spawn_xacro.launch.py'
                ])
            ]),
            launch_arguments={
                'model_name': model_name,
                'model_ns': model_ns,
                'x': "0.0",
                'y': "0.0",
                'z': "0.0",
                'r': "0.0",
                'p': "0.0",
                'Y': "0.0"

            }.items()
        )

    return [spawn_xacro]


def generate_launch_description():


    gazebo = IncludeLaunchDescription(
                PythonLaunchDescriptionSource([os.path.join(
                    get_package_share_directory('alpha_gazebo_worlds'), 
                    'launch', 'alpha_gazebo_worlds.launch.py')]),
             )
    spaw_xacro = OpaqueFunction(function = spawn_model)
    alpha_emu  = OpaqueFunction(function = start_alpha_emu)

    ld = LaunchDescription(launch_args)
    ld.add_action(spaw_xacro)
    ld.add_action(alpha_emu)

    ld.add_action(gazebo)

    return ld