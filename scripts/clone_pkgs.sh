#!/bin/bash

RETURN_PATH=$PWD

cd "$( dirname "${BASH_SOURCE[0]}" )/../ws/src"
# SRC_PATH=${REPO_LOCAL_PATH}/simulator_ws/src

git clone https://gitlab.com/sl_prak/alpha_emu.git
git clone https://gitlab.com/sl_prak/alpha_emu_gazebo_plugin.git
git clone https://gitlab.com/sl_prak/alpha_emu_msgs.git
git clone https://gitlab.com/sl_prak/alpha_gazebo_vehicles.git
git clone https://gitlab.com/sl_prak/alpha_gazebo_worlds.git

cd ${RETURN_PATH}
