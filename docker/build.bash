#!/usr/bin/env bash

# Copyright 2022 Dema Nikolay.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
EXEC_PATH=${PWD}

cd ${ROOT_DIR}

if [[ $1 = "--nvidia" ]] || [[ $1 = "-n" ]]
  then
    docker build -t alpha_gazebo -f ${ROOT_DIR}/docker/Dockerfile ${ROOT_DIR} \
                                --network=host                                \
                                --build-arg from=nvidia/opengl:1.1-glvnd-runtime-ubuntu22.04

else
    echo "[!] If you wanna use nvidia gpu, please rebuild with -n or --nvidia argument"
    docker build -t alpha_gazebo -f ${ROOT_DIR}/docker/Dockerfile ${ROOT_DIR} \
                                --network=host                                \
                                --build-arg from=ubuntu:22.04
fi

clear='\033[0m' # Clear the color after massage

if [ $? -eq 0 ]; then

    green='\033[0;32m'
    printf "${green}\nDone building. Enjoy!${clear}!\n\n"

else
    red='\033[0;31m'
    printf "${red}\nBuilding error. Smth wrong!${clear}!\n\n"
fi

cd ${EXEC_PATH}
